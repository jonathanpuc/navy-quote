import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import "./index.css";
import registerServiceWorker from "./registerServiceWorker";

import QuoteStart from "./containers/quote-start/QuoteStart";
import QuoteAboutYou from "./containers/quote-about-you/QuoteAboutYou";

const routes = (
  <BrowserRouter>
    <div>
      <Route path="/quote" component={QuoteStart} exact={true} />
      <Route path="/quote/about-you" component={QuoteAboutYou} />
    </div>
  </BrowserRouter>
);

ReactDOM.render(routes, document.getElementById("root"));
registerServiceWorker();
