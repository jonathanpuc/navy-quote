import React, { Component } from "react";

import FormDropdown from "../../components/form-entry/FormDropdown";
import Header from "../../components/header/Header";
import "./QuoteStart.css";

class QuoteStart extends Component {
  state = {
    location: "",
    amount: "",
    age: "",
    type: "",
    other: { is: "", status: "" },
    error: ""
  };
  render() {
    const state = this.state;

    const renderInputDescription = text => (
      <p style={{ marginLeft: 15, fontSize: 15 }}>{text}</p>
    );

    const renderHeaderMessage = () => {
      if (
        !this.state.location &&
        !this.state.amount &&
        !this.state.age &&
        !this.state.type &&
        !this.state.other.is &&
        !this.state.other.status
      ) {
        return (
          <h3>
            I'm Ava and I'm here to help find the best policy that suits you.
          </h3>
        );
      } else if (
        this.state.location &&
        this.state.amount &&
        this.state.age &&
        this.state.type &&
        this.state.other.is &&
        this.state.other.status
      ) {
        return <h3>Perfect! When you're ready, we can continue.</h3>;
      } else {
        return <h3>We're here to help find the best policy that suits you.</h3>;
      }
    };

    return (
      <div>
        <Header />
        <div className="container-fluid text-center">
          <div className="row header">
            <div className="col-xs-12">
              <h1>Hey there! Welcome to Navy Health.</h1>
            </div>
            <div className="col-xs-12">{renderHeaderMessage()}</div>
          </div>

          <div className="row form-container">
            <div className="col-xs-12 input-container">
              <p className="input-text">I live in</p>
              <FormDropdown
                placeholder={state.location}
                optionType="location"
                options={["VIC", "NSW", "SA", "NT", "WA"]}
                handleSelect={this.onSelect}
              />
            </div>
            {this.state.location &&
              !this.state.amount &&
              renderInputDescription("Who did you want to cover?")}
            <div className="col-xs-12 input-container">
              <p className="input-text">I want to cover</p>
              <FormDropdown
                placeholder={state.amount}
                optionType="amount"
                options={[
                  "one adult",
                  "two adults",
                  "three adults",
                  "four adults",
                  "five adults"
                ]}
                handleSelect={this.onSelect}
              />
            </div>

            {this.state.amount &&
              !this.state.age &&
              renderInputDescription(
                "Enter age of youngest adult to be covered."
              )}
            <div className="col-xs-12 input-container">
              <p className="input-text">I am</p>
              <input
                className="btn input-age"
                placeholder="30"
                value={state.age || ""}
                onChange={e => this.setState({ age: e.target.value })}
              />
              <p className="input-text" style={{ marginLeft: 15 }}>
                years of age
              </p>
            </div>

            {this.state.age &&
              !this.state.type &&
              renderInputDescription("Which cover type do you prefer?")}
            <div className="col-xs-12 input-container">
              <p className="input-text">I am after</p>
              <FormDropdown
                placeholder={state.type}
                optionType="type"
                options={[
                  "cheapest cover",
                  "effective cover",
                  "balanced cover"
                ]}
                handleSelect={this.onSelect}
              />
            </div>

            {this.state.type &&
              !this.state.other.is &&
              renderInputDescription(
                "Have you ever served in the Australian Defence Force?"
              )}
            <div className="col-xs-12 input-container">
              <FormDropdown
                placeholder={state.other.is}
                optionType="otherIs"
                options={["Yes", "No"]}
                handleSelect={this.onSelect}
              />
              <p className="input-text" style={{ marginLeft: 15 }}>
                I am
              </p>
              <FormDropdown
                placeholder={state.other.status}
                optionType="otherStatus"
                options={[
                  "an active reservist",
                  "a retired vet",
                  "something else"
                ]}
                handleSelect={this.onSelect}
              />
            </div>
          </div>
          <button
            onClick={this.submitDetails}
            className="btn-default green-bg submit-btn"
          >
            Next
          </button>
          <p>{this.state.error}</p>
        </div>
      </div>
    );
  }

  submitDetails = () => {
    let filledAllDetails = this.allDetailsFilled();
    if (isNaN(this.state.age)) {
      this.setState({
        error: "Please enter a valid age."
      });
    } else if (!filledAllDetails) {
      this.setState({
        error: "Please ensure all info is filled."
      });
    } else {
      console.log("submitted");
    }
  };

  allDetailsFilled = () => {
    if (
      this.state.location &&
      this.state.amount &&
      this.state.age &&
      this.state.type &&
      this.state.other.is &&
      this.state.other.status
    ) {
      return true;
    }
    return false;
  };

  onSelect = (option, selection) => {
    switch (option) {
      case "location":
        this.setState({
          location: selection
        });
        break;
      case "amount":
        this.setState({
          amount: selection
        });
        break;
      case "type":
        this.setState({
          type: selection
        });
        break;
      case "otherIs":
        this.setState({ other: { ...this.state.other, is: selection } });
        break;
      case "otherStatus":
        this.setState({ other: { ...this.state.other, status: selection } });
        break;
    }
  };
}

export default QuoteStart;
