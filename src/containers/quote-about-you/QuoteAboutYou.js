import React from "react";

import Header from "../../components/header/Header";
import QuoteFlowBar from "../../components/quote-flow-bar/QuoteFlowBar";
import "./QuoteAboutYou.css";

const upArrow = require("../../assets/img/up_arrow_small.png");
const lock = require("../../assets/img/lock.png");
const documentationUpload = require("../../assets/img/documentation_upload.png");
const downArrow = require("../../assets/img/down_arrow_small.png");
const options = ["Mr", "Mrs", "Ms"];

class QuoteAboutYou extends React.Component {
  state = {
    title: "",
    firstName: "",
    middleName: "",
    lastName: "",
    birthday: "",
    mobile: "",
    home: "",
    email: "",
    homeAddress: "",
    medicareCard: { number: "", expiry: "" },
    dvaCardNumber: "",
    pmKeysNumber: ""
  };

  render() {
    const titleDropdown = () => (
      <div
        className="dropdown title-dropdown"
        style={{ display: "inline-block" }}
      >
        <button
          className="btn title-dropdown-tog"
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-haspopup="true"
          aria-expanded="false"
        >
          {this.state.title ? (
            <p className="title-dropdown-placeholder">{this.state.title}</p>
          ) : (
            <p className="title-dropdown-placeholder-empty">Title</p>
          )}
          <img src={downArrow} alt="dropdown-arrow" />
        </button>
        <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
          {options.map(option => (
            <a
              className="dropdown-item"
              onClick={() => this.selectTitle(option)}
              href="#"
              key={option}
            >
              {option}
            </a>
          ))}
        </div>
      </div>
    );

    return (
      <div style={{ paddingBottom: 100 }}>
        <Header />
        <div className="container-fluid text-center">
          <div className="back-to-quote-btn">
            <div>
              <img src={upArrow} alt="up-arrow" />
            </div>
            <p>Back to quote</p>
          </div>

          <div>
            <h4>Alright, I just need a few more details</h4>
            <h3>[Name] please fill in your details below.</h3>
          </div>

          <div className="quote-about-you__form-container">
            <div className="quote-about-you__form-head">
              <div>
                <img src={lock} alt="padlock" />
                <p>Secure information input</p>
              </div>
            </div>

            <div className="personal-details-input-container">
              <div className="title-firstname-input">
                {titleDropdown()}
                <div style={{ padding: 10 }}>
                  <input
                    type="text"
                    placeholder="First name"
                    className="quote-about-you__form-input"
                    onChange={e => this.handleInputEvent("firstName", e)}
                  />
                </div>
              </div>
              <div style={{ padding: 10 }}>
                <input
                  type="text"
                  placeholder="Middle name(if applicable)"
                  className="quote-about-you__form-input "
                  onChange={e => this.handleInputEvent("middleName", e)}
                />
              </div>
              <div style={{ padding: 10 }}>
                <input
                  type="text"
                  placeholder="Last name"
                  className="quote-about-you__form-input "
                  onChange={e => this.handleInputEvent("lastName", e)}
                />
              </div>
            </div>

            <div className="birthday-input-container">
              <p>Date of birth</p>
              <div>
                <input
                  type="text"
                  placeholder="DD / MM / YYYY"
                  className="quote-about-you__form-input "
                  onChange={e => this.handleInputEvent("birthday", e)}
                />
              </div>
            </div>

            <div className="medicare-details-container">
              <p>Medicare details</p>
              <div className="medicare-card">
                <input
                  className="medicare-card-number-input"
                  type="text"
                  placeholder="Card number"
                  onChange={e => this.handleInputEvent("medicareCardNumber", e)}
                />
                <div className="medicare-card-name-container">
                  <input type="text" />
                  <p>[FIRST &amp; LAST NAME]</p>
                </div>
                <div className="medicare-card-valid-container">
                  <p>VALID TO</p>{" "}
                  <input
                    type="text"
                    placeholder="MM / YYYY"
                    onChange={e =>
                      this.handleInputEvent("medicareCardExpiry", e)
                    }
                  />
                </div>
              </div>
            </div>

            <div className="dva-details-container">
              <p>DVA Card number (optional)</p>
              <div className="dva-card">
                <div className="dva-card-number-container">
                  <p>File No.</p>{" "}
                  <input
                    type="text"
                    onChange={e => this.handleInputEvent("dvaCardNumber", e)}
                  />
                </div>
              </div>
            </div>

            <div className="pm-keys-container">
              <p>PM Keys number</p>
              <input
                type="text"
                onChange={e => this.handleInputEvent("pmKeysNumber", e)}
              />
            </div>

            <div className="documents-upload-container">
              <p>
                To confirm your reservist or DVA status please provide
                documentation (optional)
              </p>
              <div className="documents-upload-image-container">
                <img src={documentationUpload} alt="document-upload" />
                <p>Drop / find files</p>
              </div>
            </div>
          </div>

          <div className="contact-information-header">
            <div
              style={{
                backgroundColor: "blue",
                height: 50,
                width: 50,
                margin: "auto"
              }}
            >
              asdasdasd
            </div>
            <h4>Great, Thank you!</h4>
            <h3>Now I need your contact information.</h3>
          </div>

          <div className="contact-information-form-container">
            <div className="form-inputs-container">
              <div>
                <input
                  type="text"
                  placeholder="Mobile Phone Number"
                  className="quote-about-you__form-input "
                  onChange={e => this.handleInputEvent("mobile", e)}
                />
              </div>
              <div>
                <input
                  type="text"
                  placeholder="Home Phone Number (optional)"
                  className="quote-about-you__form-input "
                  onChange={e => this.handleInputEvent("home", e)}
                />
              </div>
              <div>
                <input
                  type="text"
                  placeholder="Email"
                  className="quote-about-you__form-input "
                  onChange={e => this.handleInputEvent("email", e)}
                />
              </div>
              <div>
                <input
                  type="text"
                  placeholder="Home address"
                  className="quote-about-you__form-input "
                  onChange={e => this.handleInputEvent("homeAddress", e)}
                />
              </div>
            </div>
          </div>

          <button
            onClick={this.submitDetails}
            className="btn-default green-bg submit-btn"
            style={{ width: "30%" }}
          >
            Next
          </button>
        </div>
        <div style={{ position: "fixed", left: 0, bottom: 0, width: "100%" }}>
          <QuoteFlowBar stage="about-you" />
        </div>
      </div>
    );
  }

  selectTitle = title => {
    this.setState({ title });
  };

  handleInputEvent = (input, event) => {
    switch (input) {
      case "firstName":
        this.setState({ firstName: event.target.value });
        break;
      case "middleName":
        this.setState({ middleName: event.target.value });
        break;
      case "lastName":
        this.setState({ lastName: event.target.value });
        break;
      case "birthday":
        this.setState({ birthday: event.target.value });
        break;
      case "medicareCardNumber":
        this.setState({
          medicareCard: {
            ...this.state.medicareCard,
            number: event.target.value
          }
        });
        break;
      case "medicareCardExpiry":
        this.setState({
          medicareCard: {
            ...this.state.medicareCard,
            expiry: event.target.value
          }
        });
        break;
      case "dvaCardNumber":
        this.setState({ dvaCardNumber: event.target.value });
        break;
      case "pmKeysNumber":
        this.setState({ pmKeysNumber: event.target.value });
        break;
      case "mobile":
        this.setState({ mobile: event.target.value });
        break;
      case "home":
        this.setState({ home: event.target.value });
        break;
      case "email":
        this.setState({ email: event.target.value });
        break;
      case "homeAddress":
        this.setState({ homeAddress: event.target.value });
        break;
    }
  };
}

export default QuoteAboutYou;
