import React from "react";

import "./Header.css";

const logo = require("../../assets/img/logo.png");

const Header = () => (
  <div className="header-container">
    <div>
      <img src={logo} className="logo" alt="navy health logo" />
    </div>
  </div>
);

export default Header;
