import React from "react";

import "./FormDropdown.css";

const downArrow = require("../../assets/img/down_arrow_small.png");
const FormDropdown = ({ placeholder, optionType, options, handleSelect }) => {
  return (
    <div className="dropdown " style={{ display: "inline-block" }}>
      <button
        className="btn dropdown-tog"
        type="button"
        id="dropdownMenuButton"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        {placeholder === "" ? (
          <p className="form-dropdown__placeholder-empty">{options[0]}</p>
        ) : (
          <p className="form-dropdown__placeholder">{placeholder}</p>
        )}
        <img src={downArrow} alt="dropdown-arrow" />
      </button>
      <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
        {options.map(option => (
          <a
            className="dropdown-item"
            onClick={() => handleSelect(optionType, option)}
            href="#"
            key={option}
          >
            {option}
          </a>
        ))}
      </div>
    </div>
  );
};

export default FormDropdown;
