import React from "react";

import "./QuoteFlowBar.css";

const QuoteFlowBar = ({ stage }) => (
  <div className="quote-flow-bar__container">
    <ul>
      <li className={stage === "about-you" ? "first-active" : null}>
        About you
      </li>
      <li className={stage === "rebate" ? "is-active" : null}>Rebate</li>
      <li className={stage === "payment" ? "is-active" : null}>Payment</li>
    </ul>
  </div>
);

export default QuoteFlowBar;
